import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { OptimizerRoutingModule } from './optimizer-routing.module';
import { OptimizerComponent } from './optimizer.component';

@NgModule({
  declarations: [OptimizerComponent],
  imports: [
    CommonModule,
    OptimizerRoutingModule,
    ReactiveFormsModule,
    MatCheckboxModule,
  ],
})
export class OptimizerModule {}
