import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OptimizerResolverService } from './optimizer-resolver.service';
import { OptimizerComponent } from './optimizer.component';

const routes: Routes = [
  {
    path: '',
    component: OptimizerComponent,
    resolve: {
      armorSets: OptimizerResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptimizerRoutingModule {}
