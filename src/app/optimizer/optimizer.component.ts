import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ArmorSet, ArmorSets } from '../optimizer-service/optimizer.service';

type ArmorControlsBase = { [Property in keyof ArmorSet]: FormControl };

class SetControls implements ArmorControlsBase {
  all: FormControl;
  'Chest Armor'?: FormControl;
  Gauntlets?: FormControl;
  Helm?: FormControl;
  'Leg Armor': FormControl;

  constructor() {
    this.all = new FormControl(true);
  }
}

type SetControlType = { [Property in keyof SetControls]: FormControl };

@Component({
  selector: 'app-optimizer',
  templateUrl: './optimizer.component.html',
  styleUrls: ['./optimizer.component.scss'],
})
export class OptimizerComponent implements OnInit, AfterViewInit {
  constructor(private readonly route: ActivatedRoute) {}

  armorSets?: ArmorSets;
  form!: FormGroup;

  ngOnInit(): void {
    console.log('OptimizerComponent', 'ngOnInit');
    this.route.data.subscribe((data) => {
      console.log('OptimizerComponent', 'data.subscribe');
      this.armorSets = data['armorSets'];
      console.log('OptimizerComponent', 'data.subscribe done', this.armorSets);

      const groups: any = {};

      this.armorSets!.forEach((value, key) => {
        const set_controls: SetControlType = new SetControls();
        if (value.Helm) {
          set_controls.Helm = new FormControl(true);
        }
        if (value['Chest Armor']) {
          set_controls['Chest Armor'] = new FormControl(true);
        }
        if (value.Gauntlets) {
          set_controls.Gauntlets = new FormControl(true);
        }
        if (value['Leg Armor']) {
          set_controls['Leg Armor'] = new FormControl(true);
        }
        groups[key.toString()] = new FormGroup(set_controls);
      });

      this.form = new FormGroup(groups);
    });
  }

  ngAfterViewInit(): void {
    console.log('OptimizerComponent', 'ngAfterViewInit');
  }

  armorSetFormGroup(armorSetKey: string | String): FormGroup {
    const rv = this.form.controls[armorSetKey.toString()];
    if (rv) {
      return rv as FormGroup;
    } else {
      throw new Error(
        "IllegalState: form['" + armorSetKey + "'] should have exited"
      );
    }
  }

  checkAllSelected(armorSetKey: string | String): boolean {
    const controls = this.itemCheckboxControls(armorSetKey);
    return controls.every((control) => control.value);
  }

  checkSomeSelected(armorSetKey: string | String): boolean {
    const controls = this.itemCheckboxControls(armorSetKey);
    const numSelected = controls.filter((control) => control.value).length;
    return 0 < numSelected && numSelected < controls.length;
  }

  itemCheckboxControls(armorSetKey: string | String): AbstractControl[] {
    const group = this.armorSetFormGroup(armorSetKey);
    const controls = Object.keys(group.controls)
      .filter((key) => key !== 'all')
      .map((controlKey) => group.controls[controlKey]);
    return controls;
  }

  selectAll(armorSetKey: string | String, checked: boolean): void {
    this.itemCheckboxControls(armorSetKey).forEach((control) =>
      control.setValue(checked)
    );
  }
}
