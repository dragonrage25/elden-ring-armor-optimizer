import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, shareReplay } from 'rxjs';
import { AssetLoader } from './asset-loader';

@Injectable()
export class BrowserAssetLoader implements AssetLoader {
  constructor(private readonly http: HttpClient) {}
  getAsset<T>(relative_path: string): Observable<T> {
    console.log('BrowserAssetLoader', 'getAsset', relative_path);
    return this.http.get<T>('/assets/' + relative_path).pipe(shareReplay(1));
  }
}
