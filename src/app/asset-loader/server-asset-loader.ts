import { Injectable } from '@angular/core';
import { readFile, stat } from 'fs';
import { map, Observable, Subject } from 'rxjs';
import { AssetLoader } from './asset-loader';

@Injectable()
export class ServerAssetLoader implements AssetLoader {
  private readonly assets_path = './src/assets/';
  constructor() {
    stat(this.assets_path, (err, stats) => {
      console.log('ServerAssetLoader', 'isDirectory', stats.isDirectory());
      console.log('ServerAssetLoader', stats);
    });
  }
  getAsset<T>(relative_path: string): Observable<T> {
    console.log('ServerAssetLoader', 'getAsset', relative_path);
    const result = new Subject<T>();
    readFile(this.assets_path + relative_path, 'utf8', (error, data) => {
      console.log('ServerAssetLoader', 'readFile(', relative_path, ')');
      if (error) {
        console.log('ServerAssetLoader', 'readFile', 'error', error);
        result.error(error);
      } else {
        console.log('ServerAssetLoader', 'readFile', 'success');
        result.next(data as unknown as T);
      }
      result.complete();
    });
    return result.pipe(
      map((content) => {
        if (relative_path.endsWith('.json')) {
          return JSON.parse(content as unknown as string);
        }
        return content;
      })
    );
  }
}
