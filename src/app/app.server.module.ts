import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { ASSET_LOADER } from './asset-loader/asset-loader';
import { ServerAssetLoader } from './asset-loader/server-asset-loader';

@NgModule({
  imports: [AppModule, ServerModule],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: ASSET_LOADER,
      useClass: ServerAssetLoader,
    },
  ],
})
export class AppServerModule {}
