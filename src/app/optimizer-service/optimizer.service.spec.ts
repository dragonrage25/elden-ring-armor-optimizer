import { TestBed } from '@angular/core/testing';
import { AssetLoader, ASSET_LOADER } from '../asset-loader/asset-loader';
import { OptimizerService } from './optimizer.service';

describe('OptimizerService', () => {
    let service: OptimizerService;
    let loader: AssetLoader;

    beforeEach(() => {
        loader = jasmine.createSpyObj('AssetLoader', ['getAsset']);
        TestBed.configureTestingModule({
            providers: [{ provide: ASSET_LOADER, useValue: loader }],
        });
        service = TestBed.inject(OptimizerService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
