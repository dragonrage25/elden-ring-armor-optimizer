import { convertToParamMap, Data, ParamMap, Params } from '@angular/router';
import { ReplaySubject } from 'rxjs';

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 *
 * see: https://angular.io/guide/testing-components-scenarios#activatedroutestub
 */
export class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable

  private dataSubject = new ReplaySubject<Data>();
  private paramMapSubject = new ReplaySubject<ParamMap>();
  constructor() { }

  /** The mock paramMap observable */
  readonly data = this.dataSubject.asObservable();
  readonly paramMap = this.paramMapSubject.asObservable();

  /** Set the observable's next value */
  setData(data: Data = {}) {
    this.dataSubject.next(data);
  }

  setParamMap(params: Params = {}) {
    this.paramMapSubject.next(convertToParamMap(params));
  }

}
